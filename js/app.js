let $ = document;
let mobileNavMenu = $.getElementById("mobile-nav__menu-wrapper");
let mobileMenu = $.getElementById("menu-mobile");

mobileNavMenu.addEventListener("click", menuOpenAndClose);

function menuOpenAndClose() {
  if (mobileMenu.style.left === "-50vw") {
    mobileMenu.style.left = "0";
  } else {
    mobileMenu.style.left = "-50vw";
  }
}
